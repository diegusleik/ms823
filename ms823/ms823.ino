// Ref DHT11: https://create.arduino.cc/projecthub/Arca_Ege/using-dht11-b0f365
// Ref SDS011: https://github.com/ricki-z/SDS011

// SDS011 dust sensor example
// -----------------------------
//
// By R. Zschiegner (rz@madavi.de).
// April 2016

// ms823
// version = 1
// version date = 18.02.2020

// conexiones
// pin data DHT22 --> 2
// 10KR entre DHT22 pin 2 y vcc
// en shield: pin 3 --> L2
// en shield: pin 4 --> L1
// sds tx SDS011 --> 8
// sds rx SDS011 --> 9
// transistor MPS A13. vcc a C + 220R entre pin 7 y base + emisor a vcc SDS11
// todos los vcc (positivos) --> vin
// gnd (negativo) --> gnd

// modificar en cada instalación
#define dataLogger_ID "datalogger_1" // nombre del dispositivo
#define SEGUNDOS_ESPERA_ENTRE_LECTURAS 10
#define MEDICIONES_POR_LECTURA 3 // número de mediciones por cada lectura
#define SEGUNDOS_ESPERA_ENTRE_ERRORES_DE_MEDICION 1
#define SEGUNDOS_ESPERA_ENTRE_LOOPS 10 // para ahorro de energía. Default = 10
#define resetDataFromSd false // true si quieres borrar la info previa del archivo de datos y de log al iniciar el módulo. Default = false
#define debugging false // Default = false

// deep config
#define REPEATS_ON_ERROR 5 // Default = 5
#define WAIT_FOR_SDS_ON 5 // seconds. Default = 5
#define SD_LED_PIN_BLINK_INTERVAL 500 // milliseconds

// Cargar librerías
#include <SD.h>
#include <Wire.h>
#include <NovaSDS011.h> // NovaSDAS011
#include <DHT.h> // Adafruit
#include <RTClib.h> // Adafruit
#include <Chrono.h> // Thomas Frederick

// Pins
#define DHT_PIN 2
#define SD_LED_PIN 3
#define SDS_LED_PIN 4
#define SDS_POWER_PIN 7
#define SDS_PIN_RX 8
#define SDS_PIN_TX 9
#define SD_PIN 10

// Variables
float pm10, pm25;
#define ARCHIVO_SALIDA "data.csv"
#define ARCHIVO_LOG "error.log"
#define DHTTYPE DHT22
char data[100];
char dataFloatTemp[10];
char dataFloatHum[10];
bool sdsIsOn = false;
int repeat = 0;
QuerryError qe;
File file;

// Objetos
RTC_DS1307 RTC;
NovaSDS011 sds011;
DHT dht(DHT_PIN, DHTTYPE);
Chrono ms823Chrono(Chrono::SECONDS);

void setup ()
{
  // Iniciar dispositivos
  if (debugging) {
    Serial.begin(9600);
  }

  Wire.begin();
  sds011.begin(SDS_PIN_RX, SDS_PIN_TX);
  dht.begin();

  // Inicializar pins
  pinMode(SD_LED_PIN, OUTPUT);
  digitalWrite(SD_LED_PIN, LOW);
  pinMode(SDS_LED_PIN, OUTPUT);
  digitalWrite(SDS_LED_PIN, LOW);

  pinMode(SDS_POWER_PIN, OUTPUT);
  digitalWrite(SDS_POWER_PIN, LOW);

  // Inicializar hora RTC
  if (! RTC.begin()) {
    writeDebug(F("Couldn't find RTC"));
    while (1);
  } // end rtc.begin()

  if (! RTC.isrunning() || false) { // cambiar false x true para forzar ajuste de hora. Devolver inmediatamente a false y vover a cargar para evitar reinicio de hora
    writeDebug(F("RTC is NOT running!"));
    RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

  // see if the card is present and can be initialized:
  pinMode(SD_PIN, OUTPUT);
  if (!SD.begin(SD_PIN)) {
    writeDebug(F("Card failed, or not present"));
    while (1);
  } else {
    writeDebug(F("Card initialized."));
  }

  // crear los archivos si no existe.
  if (!SD.exists(ARCHIVO_SALIDA)) {
    file = SD.open(ARCHIVO_SALIDA, O_WRITE | O_CREAT);
    file.close();
  }
  if (!SD.exists(ARCHIVO_LOG)) {
    file = SD.open(ARCHIVO_LOG, O_WRITE | O_CREAT);
    file.close();
  }
  // resetear si es solicitado
  if (resetDataFromSd) {
    SD.remove(ARCHIVO_SALIDA);
    SD.remove(ARCHIVO_LOG);
    file = SD.open(ARCHIVO_LOG, O_WRITE | O_CREAT);
    file.close();
    writeDebug(F("Data file deleted ."));
  }

  // start timer
  ms823Chrono.restart();
}

void loop ()
{
  if (ms823Chrono.hasPassed(SEGUNDOS_ESPERA_ENTRE_LECTURAS)) {
    // reinicio el chrono
    ms823Chrono.restart();

    // Tomo una lectura
    powerSDS(true); // Enciendo el SDS

    int repeticion_medicion = 1; // reinicio repeticiones de medición
    
    while (repeticion_medicion <= MEDICIONES_POR_LECTURA) { // cada lectura consiste en MEDICIONES_POR_LECTURA mediciones
      
      repeat = 0; // reinicio repeticiones de intentos de medición
      
      qe = sds011.queryData(pm25, pm10); // tomo una medición
      
      while (qe != QuerryError::no_error && repeat < REPEATS_ON_ERROR) { // si hay error de medición y me quedan intentos
        writeDebug("Error medicion PM");
        delay(SEGUNDOS_ESPERA_ENTRE_ERRORES_DE_MEDICION * 1000); // espero
        qe = sds011.queryData(pm25, pm10); // intento medir nuevamente
        repeat++; // aunmento contador
      }
      
      if (qe != QuerryError::no_error && repeat == REPEATS_ON_ERROR) { // si hay error de medición y intenté leer REPEATS_ON_ERROR veces
        report_log(F("sds_read_error")); // report error to the log
        break; // salgo
      }
      
      // si el sensor no entrega error, entonces guardo el dato
      file = SD.open(ARCHIVO_SALIDA, O_RDWR | O_APPEND);
      
      if (!file) {
        writeDebug(F("Error escribiendo en la SDS")); // report error to the log
        break; // salgo
      }

      // archivo abierto, escribo y cierro     
      DateTime now = RTC.now();
      dtostrf(dht.readTemperature(), 4, 2, dataFloatTemp);
      dtostrf(dht.readHumidity(), 4, 2, dataFloatHum);
      sprintf(data, "%s,%d,%d-%d-%d %d:%d:%d,%s,%s,%d,%d", dataLogger_ID, repeticion_medicion, now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second(), dataFloatHum, dataFloatTemp, (int) pm25, (int) pm10);
      file.println(data);
      file.close();

      writeDebug(F("Datos escritos al archivo de salida"));
      writeDebug(data);
      repeticion_medicion++;
    }

    powerSDS(false); // Apago el SDS

    animarLedEscritura(repeticion_medicion);
  } else {
    delay(SEGUNDOS_ESPERA_ENTRE_LOOPS * 1000); // Ahorro de energía
  }
}

void report_log(String errorMessage) {
  file = SD.open(ARCHIVO_LOG, O_RDWR | O_APPEND);
  if (file) {
    DateTime now = RTC.now();
    sprintf(data, "%s,%d-%d-%d %d:%d:%d,%s", dataLogger_ID, now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second(), errorMessage);
    file.println(data);
    file.close();
  }
  writeDebug(errorMessage);
}

void powerSDS(bool turnOn) {
  if (turnOn == sdsIsOn)
    return;
  if (turnOn) {
    digitalWrite(SDS_POWER_PIN, HIGH);
    digitalWrite(SDS_LED_PIN, HIGH);
    delay(WAIT_FOR_SDS_ON * 1000);
  } else {
    digitalWrite(SDS_POWER_PIN, LOW);
    digitalWrite(SDS_LED_PIN, LOW);
  }
  sdsIsOn = turnOn;
}

void animarLedEscritura(int veces)
{
  for (int r = 0; r < veces; r++) {
    digitalWrite(SD_LED_PIN, HIGH);
    delay(SD_LED_PIN_BLINK_INTERVAL);
    digitalWrite(SD_LED_PIN, LOW);
    delay(SD_LED_PIN_BLINK_INTERVAL);
  }
}

void writeDebug(String mensaje) {
  if (debugging) {
    Serial.println(mensaje);
  }
}

void writeDebugNoNewline(String mensaje) {
  if (debugging) {
    Serial.print(mensaje);
  }
}
